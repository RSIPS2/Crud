﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace CrudApplication
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "GetItems",
                routeTemplate: "api/GetItems/{typeString}/{count}/{skip}/{selectByUserOrProductID}/{selectOnlyIdsString}",
                defaults: new { controller = "Values", action = "GetItems", selectByUserOrProductID = RouteParameter.Optional, selectOnlyIdsString = RouteParameter.Optional }

            );

            config.Routes.MapHttpRoute(
                name: "GetItemById",
                routeTemplate: "api/GetItemById/{typeString}/{id}",
                defaults: new { controller = "Values", action = "GetItemById" }

            );

            config.Routes.MapHttpRoute(
                name: "GetUserByProviderId",
                routeTemplate: "api/GetUserByProviderId/{providerName}/{userId}",
                defaults: new { controller = "Values", action = "GetUserByProviderId" }

            );


            config.Routes.MapHttpRoute(
                name: "FindProducts",
                routeTemplate: "api/FindProducts/{query}/{count}/{skip}",
                defaults: new { controller = "Values", action = "FindProducts", count = RouteParameter.Optional, skip = RouteParameter.Optional }

            );

            config.Routes.MapHttpRoute(
                name: "AddOrUpdate",
                routeTemplate: "api/AddOrUpdate/{typeString}",
                defaults: new { controller = "Values", action = "AddOrUpdate" }

            );

            config.Routes.MapHttpRoute(
                name: "Delete",
                routeTemplate: "api/Delete/{typeString}/{idsString}",
                defaults: new { controller = "Values", action = "Delete" }

            );

            config.Routes.MapHttpRoute(
                name: "SellProduct",
                routeTemplate: "api/SellProduct/{userId}",
                defaults: new { controller = "Values", action = "SellProduct" }

            );

            config.Routes.MapHttpRoute(
                name: "GetSubByBookId",
                routeTemplate: "api/GetSubByBookId/{bookId}",
                defaults: new { controller = "Values", action = "GetSubByBookId" }

            );

            config.Routes.MapHttpRoute(
                name: "GetUserByName",
                routeTemplate: "api/GetUserByName/{email}",
                defaults: new { controller = "Values", action = "GetUserByName" }

            );

            config.Routes.MapHttpRoute(
                name: "MapByAction",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
                );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }

            );

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
