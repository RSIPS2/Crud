﻿using RSICommons.Helpers;
using System;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CrudApplication.App_Start
{
    public class RequireHttpsAttribute : AuthorizationFilterAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.Request.RequestUri.Scheme != Uri.UriSchemeHttps)
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                {
                    ReasonPhrase = "HTTPS Required"
                };
            }
            else
            {
                var a = TokenHelper.ReadFromFile();
                if (actionContext.Request.Headers.Authorization != null && actionContext.Request.Headers.Authorization.Parameter == "OcguCWTZBjTIoM5Ol8dZCiFBw4mHsDiTBjWjfq9X=")
                {
                    base.OnAuthorization(actionContext);
                }
                else if ((a != null || a.Any()) && actionContext.Request.Headers.Authorization != null && a.Contains(actionContext.Request.Headers.Authorization.Parameter))
                {
                    a.Remove(actionContext.Request.Headers.Authorization.Parameter);
                    TokenHelper.SaveToFile(a);
                    base.OnAuthorization(actionContext);
                }
                else
                {
                    actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden)
                    {
                        ReasonPhrase = "Access Denied"
                    };
                }
            }
        }
    }
}