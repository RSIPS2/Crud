﻿using RSICommons.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace CrudApplication.App_Start
{
    public class AuthorizationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Response.StatusCode != System.Net.HttpStatusCode.Forbidden)
            {
                string key = TokenHelper.GeneratePswd();
                var a = TokenHelper.ReadFromFile();
                a = a == null ? new List<string>() : a;
                a.Add(key);

                TokenHelper.SaveToFile(a);

                actionExecutedContext.Response.Headers.TryAddWithoutValidation("Auth", key);
            }
            base.OnActionExecuted(actionExecutedContext);
        }
    }
}