﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace CrudApplication.Controllers
{
    public static class MailService
    {
        public static async Task SendMail(MailViewModel message)
        {
            try
            {
                var msg = new MailMessage(WebConfigurationManager.AppSettings["SMTPEmail"], message.Mail);
                msg.Body = message.Content;
                msg.Subject = message.Title + " " + message.Name + " " + message.Surname;
                msg.From = new MailAddress(WebConfigurationManager.AppSettings["SMTPEmail"]);
                msg.ReplyToList.Add(new MailAddress(message.Mail));


                using (var smtp = new SmtpClient())
                {
                    smtp.Credentials = new System.Net.NetworkCredential(WebConfigurationManager.AppSettings["SMTPUsername"], WebConfigurationManager.AppSettings["SMTPPassword"]);
                    smtp.Host = WebConfigurationManager.AppSettings["SMTPHost"];
                    var port = 0;
                    port  = int.TryParse(WebConfigurationManager.AppSettings["SMTPPort"], out port) ? port : 465;
                    smtp.Port = port;
                    smtp.EnableSsl = true;
                    await smtp.SendMailAsync(msg);
                }

                return;

            }
            catch (Exception ex)
            {
                return;
            }
        }
    }

    public class MailViewModel
    {
        public string Content { get; set; }
        public string Mail { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Title { get; set; }
    }

    public enum EmailStatus
    {
        Sent,
        Error
    }
}