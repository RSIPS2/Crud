﻿using CrudApplication.App_Start;
using CrudApplication.Models;
using Newtonsoft.Json.Linq;
using RSICommons.CommonModels;
using RSICommons.CommonModels.DTO;
using RSICommons.CommonModels.DTO.Enums;
using RSICommons.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CrudApplication.Controllers
{
    public class ValuesController : ApiController
    {

        public HttpResponseMessage GetValues()
        {
            var elo = new List<string>() { "Maciek", "Janek", "Piotrek" };

            try
            {
                var context = new RSIContext();
                var person2 = new Models.Person()
                {
                    Address = "to jest adres",
                    Email = "haha@wp.pl",
                    FirstName = "Piotr",
                    LastName = "Gornik",
                    OAuth2IdFacebook = "ggfhdfgdgfdgsvearwrreer454654672353424x22423z424234323v5",
                    PersonProducts = new List<Models.PersonProduct>(),
                    PhoneNumber = "2332343453",
                    Subscriptions = new List<Models.Subscription>(),

                };

                context.People.Add(person2);
                context.SaveChanges();
                var person = context.People.FirstOrDefault();
                var product = new Models.Product()
                {
                    Amount = 10,
                    Title = "Tytuł",
                    Author = "Pan włądysław",
                    Description = "Opis ksążki",
                    PublishDate = DateTime.Now.ToString(),
                    Genre = "Fantastyka",
                    PageCount = 1000,
                    Price = "50",
                    Publisher = "Helion",
                    Language = "sdad",
                    OriginalLanguage = "sdadd",
                    Thumbnail = "sdsdsdd",
                    TranslatedBy = "dsdsd"
                };
                context.Products.Add(product);
                context.SaveChanges();

                var productPerson = new Models.PersonProduct()
                {
                    Amount = 2,
                    BuyTime = DateTime.Now,
                    Price = "100",
                    Person = person,
                    Product = product
                };
                context.ProductPersons.Add(productPerson);


                var sub = new Models.Subscription()
                {
                    Person = person,
                    Product = product,
                    Type = SubscriptionType.Product
                };
                context.Subscriptions.Add(sub);
                context.SaveChanges();
                //context.Entry(person).State = System.Data.Entity.EntityState.Modified;
            }
            catch (Exception ex)
            {


            }

            return Request.CreateResponse(HttpStatusCode.OK, new ApiResult { status = true, result = elo });

        }

        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage GetItems(string typeString, int count = 0, int skip = 0, string selectByUserOrProductID = "", string selectOnlyIdsString = "")
        {
            var dbContext = new RSIContext();
            try
            {
                var type = typeString.ToEnum<DTOTypes>();
                var selectOnlyIds = selectOnlyIdsString.Split(new string[] { "<==>" }, StringSplitOptions.None);
                if (selectOnlyIdsString == "")
                    selectOnlyIds = new string[0];

                switch (type)
                {
                    case DTOTypes.Default:

                        return Request.CreateResponse(HttpStatusCode.BadRequest, new ApiResult { status = false, result = "No such object in Entity" });

                    case DTOTypes.Person:

                        var personPredicate = selectOnlyIds != null && selectOnlyIds.Any() ? dbContext.People.Where(p => selectOnlyIds.Contains(p.Id.ToString())) : dbContext.People;

                        var people = personPredicate.Select(p => new RSICommons.CommonModels.DTO.Person()
                        {
                            Id = p.Id,
                            Address = p.Address,
                            Email = p.Email,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            OAuth2IdFacebook = p.OAuth2IdFacebook,
                            OAuth2IdGoogle = p.OAuth2IdGoogle,
                            OAuth2IdTwitter = p.OAuth2IdTwitter,
                            PhoneNumber = p.PhoneNumber,
                        });

                        if (skip != 0)
                            people = people.Skip(skip);

                        if (count != 0)
                            people = people.Take(count);

                        return Request.CreateResponse(HttpStatusCode.OK, people.ToList());

                    case DTOTypes.Product:

                        var productPredicate = selectOnlyIds != null && selectOnlyIds.Any() ? dbContext.Products.Where(p => selectOnlyIds.Contains(p.Id.ToString())) : dbContext.Products;

                        var product = productPredicate.Select(p => new RSICommons.CommonModels.DTO.Product()
                        {
                            Amount = p.Amount,
                            Author = p.Author,
                            Description = p.Description,
                            Genre = p.Genre,
                            Id = p.Id,
                            PageCount = p.PageCount,
                            Price = p.Price,
                            PublishDate = p.PublishDate,
                            Publisher = p.Publisher,
                            Title = p.Title,
                            Language = p.Language,
                            OriginalLanguage = p.OriginalLanguage,
                            Thumbnail = p.Thumbnail,
                            TranslatedBy = p.TranslatedBy

                        });

                        if (skip != 0)
                            product = product.Skip(skip);

                        if (count != 0)
                            product = product.Take(count);

                        return Request.CreateResponse(HttpStatusCode.OK, product.ToList());

                    case DTOTypes.Subscription:


                        var subscriptionsPredicate = selectByUserOrProductID.Equals(Guid.Empty.ToString()) || selectByUserOrProductID.Equals(string.Empty) ? dbContext.Subscriptions : dbContext.Subscriptions.Where(s => s.PersonId.ToString() == selectByUserOrProductID);

                        subscriptionsPredicate = selectOnlyIds != null && selectOnlyIds.Any() ? subscriptionsPredicate.Where(p => selectOnlyIds.Contains(p.Id.ToString())) : subscriptionsPredicate;

                        var sub = subscriptionsPredicate.Select(s => new RSICommons.CommonModels.DTO.Subscription()
                        {
                            Id = s.Id,
                            ProductId = s.ProductId,
                            UserId = s.PersonId,
                            Product = new RSICommons.CommonModels.DTO.Product()
                            {
                                Amount = s.Product.Amount,
                                Author = s.Product.Author,
                                Description = s.Product.Description,
                                Genre = s.Product.Genre,
                                Id = s.Product.Id,
                                PageCount = s.Product.PageCount,
                                Price = s.Product.Price,
                                PublishDate = s.Product.PublishDate,
                                Publisher = s.Product.Publisher,
                                Title = s.Product.Title,
                                Language = s.Product.Language,
                                OriginalLanguage = s.Product.OriginalLanguage,
                                Thumbnail = s.Product.Thumbnail,
                                TranslatedBy = s.Product.TranslatedBy
                            }
                        });

                        return Request.CreateResponse(HttpStatusCode.OK, sub.ToList());

                    case DTOTypes.PersonProduct:

                        var peronProductPredicate = selectByUserOrProductID.Equals(Guid.Empty.ToString()) || selectByUserOrProductID.Equals(string.Empty) ? dbContext.ProductPersons : dbContext.ProductPersons.Where(pp => selectByUserOrProductID.Equals(pp.PersonId.ToString()));
                        var idss = new List<Guid>();
                        foreach (var item in selectOnlyIds)
                        {
                            idss.Add(new Guid(item));
                        }
                        peronProductPredicate = selectOnlyIds != null && idss.Any() ? peronProductPredicate.Where(p => idss.Contains(p.Id)) : peronProductPredicate;

                        var personProduct = peronProductPredicate.Select(pp => new RSICommons.CommonModels.DTO.PersonProduct()
                        {
                            Amount = pp.Amount,
                            BuyTime = pp.BuyTime,
                            Id = pp.Id,
                            Price = pp.Price,
                            PersonId = pp.PersonId,
                            ProductId = pp.ProductId,
                            Product = new RSICommons.CommonModels.DTO.Product()
                            {
                                Amount = pp.Product.Amount,
                                Author = pp.Product.Author,
                                Description = pp.Product.Description,
                                Genre = pp.Product.Genre,
                                Id = pp.Product.Id,
                                PageCount = pp.Product.PageCount,
                                Price = pp.Product.Price,
                                PublishDate = pp.Product.PublishDate,
                                Publisher = pp.Product.Publisher,
                                Title = pp.Product.Title,
                                Language = pp.Product.Language,
                                OriginalLanguage = pp.Product.OriginalLanguage,
                                Thumbnail = pp.Product.Thumbnail,
                                TranslatedBy = pp.Product.TranslatedBy
                            },
                        });

                        return Request.CreateResponse(HttpStatusCode.OK, personProduct.ToList());
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbContext.Dispose();
            }

            return Request.CreateResponse(HttpStatusCode.NotFound);
        }

        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage GetSubByBookId(string bookId)
        {
            var dbContext = new RSIContext();
            try
            {
                var subscriptionsPredicate = bookId.Equals(Guid.Empty.ToString()) || bookId.Equals(string.Empty) ? dbContext.Subscriptions : dbContext.Subscriptions.Where(s => s.PersonId.ToString() == bookId);

                var sub = subscriptionsPredicate.Select(s => new RSICommons.CommonModels.DTO.Subscription()
                {
                    Id = s.Id,
                    ProductId = s.ProductId,
                    UserId = s.PersonId,
                    Product = new RSICommons.CommonModels.DTO.Product()
                    {
                        Amount = s.Product.Amount,
                        Author = s.Product.Author,
                        Description = s.Product.Description,
                        Genre = s.Product.Genre,
                        Id = s.Product.Id,
                        PageCount = s.Product.PageCount,
                        Price = s.Product.Price,
                        PublishDate = s.Product.PublishDate,
                        Publisher = s.Product.Publisher,
                        Title = s.Product.Title,
                        Language = s.Product.Language,
                        OriginalLanguage = s.Product.OriginalLanguage,
                        Thumbnail = s.Product.Thumbnail,
                        TranslatedBy = s.Product.TranslatedBy
                    }
                });

                return Request.CreateResponse(HttpStatusCode.OK, sub.ToList());
            }
            catch (Exception)
            {

            }
            finally
            {
                dbContext.Dispose();
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage GetItemById(string typeString, string id)
        {
            var dbContext = new RSIContext();
            try
            {
                var type = typeString.ToEnum<DTOTypes>();
                switch (type)
                {
                    case DTOTypes.Default:

                        return Request.CreateResponse(HttpStatusCode.BadRequest);

                    case DTOTypes.Person:

                        var p = dbContext.People.FirstOrDefault(x => x.Id.ToString().Equals(id));

                        var people = new RSICommons.CommonModels.DTO.Person()
                        {
                            Id = p.Id,
                            Address = p.Address,
                            Email = p.Email,
                            FirstName = p.FirstName,
                            LastName = p.LastName,
                            OAuth2IdFacebook = p.OAuth2IdFacebook,
                            OAuth2IdGoogle = p.OAuth2IdGoogle,
                            OAuth2IdTwitter = p.OAuth2IdTwitter,
                            PhoneNumber = p.PhoneNumber,
                        };

                        return Request.CreateResponse(HttpStatusCode.OK, people);

                    case DTOTypes.Product:

                        var pr = dbContext.Products.FirstOrDefault(x => x.Id.ToString().Equals(id));

                        var product = new RSICommons.CommonModels.DTO.Product()
                        {
                            Amount = pr.Amount,
                            Author = pr.Author,
                            Description = pr.Description,
                            Genre = pr.Genre,
                            Id = pr.Id,
                            PageCount = pr.PageCount,
                            Price = pr.Price,
                            PublishDate = pr.PublishDate,
                            Publisher = pr.Publisher,
                            Title = pr.Title,
                            Language = pr.Language,
                            OriginalLanguage = pr.OriginalLanguage,
                            Thumbnail = pr.Thumbnail,
                            TranslatedBy = pr.TranslatedBy
                        };

                        return Request.CreateResponse(HttpStatusCode.OK, product);

                    case DTOTypes.Subscription:

                        var s = dbContext.Subscriptions.FirstOrDefault(x => x.Id.ToString().Equals(id));

                        var sub = new RSICommons.CommonModels.DTO.Subscription()
                        {
                            Id = s.Id,
                            ProductId = s.ProductId,
                            UserId = s.PersonId,
                            Product = new RSICommons.CommonModels.DTO.Product()
                            {
                                Amount = s.Product.Amount,
                                Author = s.Product.Author,
                                Description = s.Product.Description,
                                Genre = s.Product.Genre,
                                Id = s.Product.Id,
                                PageCount = s.Product.PageCount,
                                Price = s.Product.Price,
                                PublishDate = s.Product.PublishDate,
                                Publisher = s.Product.Publisher,
                                Title = s.Product.Title,
                                Language = s.Product.Language,
                                OriginalLanguage = s.Product.OriginalLanguage,
                                Thumbnail = s.Product.Thumbnail,
                                TranslatedBy = s.Product.TranslatedBy
                            }
                        };

                        return Request.CreateResponse(HttpStatusCode.OK, sub);

                    case DTOTypes.PersonProduct:

                        var pp = dbContext.ProductPersons.FirstOrDefault(x => x.Id.ToString().Equals(id));

                        var personProduct = new RSICommons.CommonModels.DTO.PersonProduct()
                        {
                            Amount = pp.Amount,
                            BuyTime = pp.BuyTime,
                            Id = pp.Id,
                            Price = pp.Price,
                            PersonId = pp.PersonId,
                            ProductId = pp.ProductId,
                            Product = new RSICommons.CommonModels.DTO.Product()
                            {
                                Amount = pp.Product.Amount,
                                Author = pp.Product.Author,
                                Description = pp.Product.Description,
                                Genre = pp.Product.Genre,
                                Id = pp.Product.Id,
                                PageCount = pp.Product.PageCount,
                                Price = pp.Product.Price,
                                PublishDate = pp.Product.PublishDate,
                                Publisher = pp.Product.Publisher,
                                Title = pp.Product.Title,
                                Language = pp.Product.Language,
                                OriginalLanguage = pp.Product.OriginalLanguage,
                                Thumbnail = pp.Product.Thumbnail,
                                TranslatedBy = pp.Product.TranslatedBy
                            },
                        };

                        return Request.CreateResponse(HttpStatusCode.OK, personProduct);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbContext.Dispose();
            }

            return Request.CreateResponse(HttpStatusCode.OK, new ApiResult { status = true, result = null });
        }

        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage FindProducts(string query, int count = 0, int skip = 0)
        {
            var querySplited = query.Split(new string[] { "<==>" }, StringSplitOptions.None);
            var dbContext = new RSIContext();
            try
            {
                var products = dbContext.Products.Where(p => querySplited.Any(x => p.Author.Contains(x) || p.Description.Contains(x) || p.Genre.Contains(x) || p.Publisher.Contains(x) || p.Title.Contains(x)));

                if (skip != 0)
                    products = products.Skip(skip);

                if (count != 0)
                    products = products.Take(count);

                return Request.CreateResponse(HttpStatusCode.OK, products.ToList());
            }
            catch (Exception ex)
            {
            }
            finally
            {
                dbContext.Dispose();
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpDelete]
        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage Delete(string typeString, string idsString)
        {
            var dbContext = new RSIContext();
            try
            {
                var type = typeString.ToEnum<DTOTypes>();
                var ids = idsString.Split(new string[] { "<==>" }, StringSplitOptions.None);
                if (idsString == "")
                    return Request.CreateResponse(HttpStatusCode.BadRequest); ;

                switch (type)
                {
                    case DTOTypes.Default:

                        break;

                    case DTOTypes.Person:

                        var people = dbContext.People.Where(p => ids.Contains(p.Id.ToString()));
                        dbContext.People.RemoveRange(people);
                        dbContext.SaveChanges();

                        break;

                    case DTOTypes.Product:

                        var products = dbContext.Products.Where(p => ids.Contains(p.Id.ToString()));
                        dbContext.Products.RemoveRange(products);
                        dbContext.SaveChanges();

                        break;

                    case DTOTypes.Subscription:

                        var subs = dbContext.Subscriptions.Where(p => ids.Contains(p.Id.ToString()));
                        dbContext.Subscriptions.RemoveRange(subs);
                        dbContext.SaveChanges();

                        break;

                    case DTOTypes.PersonProduct:

                        var pp = dbContext.ProductPersons.Where(p => ids.Contains(p.Id.ToString()));
                        dbContext.ProductPersons.RemoveRange(pp);
                        dbContext.SaveChanges();

                        break;
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception)
            {

            }
            finally
            {
                dbContext.Dispose();
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpPost]
        [AuthorizationActionFilter]
        [RequireHttps]
        public async Task<HttpResponseMessage> AddOrUpdate(string typeString, [FromBody] JObject value)
        {
            var dbContext = new RSIContext();
            try
            {
                var type = typeString.ToEnum<DTOTypes>();
                switch (type)
                {
                    case DTOTypes.Default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "ObjectType is invalid");

                    case DTOTypes.Person:

                        var personDTO = value.ToObject<RSICommons.CommonModels.DTO.Person>();
                        Models.Person user = null;
                        if (personDTO != null && personDTO.Id != Guid.Empty)
                            user = dbContext.People.FirstOrDefault(p => p.Id == personDTO.Id);
                        if (user == null)
                            user = new Models.Person();

                        user.Address = personDTO.Address;
                        user.Email = personDTO.Email;
                        user.FirstName = personDTO.FirstName;
                        user.LastName = personDTO.LastName;
                        user.OAuth2IdFacebook = personDTO.OAuth2IdFacebook;
                        user.OAuth2IdGoogle = personDTO.OAuth2IdGoogle;
                        user.OAuth2IdTwitter = personDTO.OAuth2IdTwitter;
                        user.PhoneNumber = personDTO.PhoneNumber;

                        if (user.Id != Guid.Empty)
                        {
                            dbContext.Entry(user).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.People.Add(user);
                            dbContext.SaveChanges();
                        }

                        break;

                    case DTOTypes.Product:

                        var productDTO = value.ToObject<RSICommons.CommonModels.DTO.Product>();
                        Models.Product product = null;
                        if (productDTO != null && productDTO.Id != Guid.Empty)
                            product = dbContext.Products.FirstOrDefault(p => p.Id == productDTO.Id);
                        if (product == null)
                            product = new Models.Product();

                        if (product.Amount == 0 && productDTO.Amount > 0)
                        {
                            var people = dbContext.Subscriptions.Where(s => s.ProductId == product.Id).Select(ss => ss.Person).ToList();
                            if (people != null && people.Any())
                            {
                                //Send Emails
                                try
                                {
                                    foreach (var person in people)
                                    {
                                        await MailService.SendMail(new MailViewModel()
                                        {
                                            Mail = person.Email,
                                            Name = person.FirstName,
                                            Surname = person.LastName,
                                            Title = "Książka którą obserwujesz jest dostępna!",
                                            Content = $"Witaj! \n {person.FirstName} {person.LastName}, \n Książka {product.Title} jest już dostępna!"
                                        });
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                        }
                        product.Amount = productDTO.Amount;
                        product.Author = productDTO.Author;
                        product.Description = productDTO.Description;
                        product.Genre = productDTO.Genre;
                        product.Language = productDTO.Language;
                        product.OriginalLanguage = productDTO.OriginalLanguage;
                        product.PageCount = productDTO.PageCount;
                        product.Price = productDTO.Price;
                        product.PublishDate = productDTO.PublishDate;
                        product.Publisher = productDTO.Publisher;
                        product.Thumbnail = productDTO.Thumbnail;
                        product.Title = productDTO.Title;
                        product.TranslatedBy = productDTO.TranslatedBy;

                        if (productDTO.Id != Guid.Empty && product.Id != Guid.Empty)
                        {
                            dbContext.Entry(product).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.Products.Add(product);
                            dbContext.SaveChanges();
                        }

                        break;

                    case DTOTypes.Subscription:

                        var subDTO = value.ToObject<RSICommons.CommonModels.DTO.Subscription>();
                        Models.Subscription sub = null;
                        if (subDTO != null && subDTO.Id != Guid.Empty)
                            sub = dbContext.Subscriptions.FirstOrDefault(p => p.Id == subDTO.Id);
                        if (sub == null)
                            sub = new Models.Subscription();

                        sub.PersonId = subDTO.ProductId;
                        sub.Type = subDTO.Type;
                        sub.ProductId = subDTO.ProductId;

                        if (sub.Id != Guid.Empty)
                        {
                            dbContext.Entry(sub).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.Subscriptions.Add(sub);
                            dbContext.SaveChanges();
                        }

                        break;

                    case DTOTypes.PersonProduct:

                        var ppDTO = value.ToObject<RSICommons.CommonModels.DTO.PersonProduct>();
                        Models.PersonProduct pp = null;
                        if (ppDTO != null && ppDTO.Id != Guid.Empty)
                            pp = dbContext.ProductPersons.FirstOrDefault(p => p.Id == ppDTO.Id);
                        if (pp == null)
                            pp = new Models.PersonProduct();
                                              
                        pp.Amount = ppDTO.Amount;
                        pp.PersonId = ppDTO.PersonId;
                        pp.Price = ppDTO.Price;
                        pp.ProductId = ppDTO.ProductId;
                        pp.BuyTime = ppDTO.BuyTime;

                        if (pp.Id != Guid.Empty)
                        {
                            dbContext.Entry(pp).State = System.Data.Entity.EntityState.Modified;
                            dbContext.SaveChanges();
                        }
                        else
                        {
                            dbContext.ProductPersons.Add(pp);
                            dbContext.SaveChanges();
                        }

                        break;
                }
                var rr = Request.CreateResponse(HttpStatusCode.OK);
                rr.Headers.Add("Auth", "OcguCWTZBjTIoM5Ol8dZCiFBw4mHsDiTBjWjfq9X=");
                return rr;
            }
            catch (Exception)
            {
            }
            finally
            {
                dbContext.Dispose();
            }
            var r = Request.CreateResponse(HttpStatusCode.BadRequest);
            r.Headers.Add("Auth", "OcguCWTZBjTIoM5Ol8dZCiFBw4mHsDiTBjWjfq9X=");
            return r;
        }

        [HttpPost]
        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage SellProduct(string userId, [FromBody]IEnumerable<Cart> cart)
        {
            var dbContext = new RSIContext();
            var userGUID = new Guid(userId);

            try
            {
                var sold = new List<Models.PersonProduct>();
                foreach (var item in cart)
                {
                    var price = dbContext.Products.FirstOrDefault(p => p.Id.Equals(item.ProductId))?.Price;
                    if (price != null)
                        sold.Add(new Models.PersonProduct()
                        {
                            Amount = item.Count,
                            BuyTime = DateTime.UtcNow,
                            PersonId = userGUID,
                            ProductId = item.ProductId,
                            Price = price,
                        });
                }
                dbContext.ProductPersons.AddRange(sold);
                //dbContext.Entry(sold).State = System.Data.Entity.EntityState.Added;
                dbContext.SaveChanges();

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {

            }
            finally
            {
                dbContext.Dispose();
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage GetUserByProviderId(string providerName, string userId)
        {
            var dbContext = new RSIContext();
            Models.Person user = null;
            try
            {
                Provider provider = providerName.ToEnum<Provider>();

                switch (provider)
                {
                    case Provider.Default:
                        break;
                    case Provider.Google:
                        user = dbContext.People.FirstOrDefault(p => p.OAuth2IdGoogle.Equals(userId));
                        break;
                    case Provider.Twitter:
                        user = dbContext.People.FirstOrDefault(p => p.OAuth2IdTwitter.Equals(userId));
                        break;
                    case Provider.Facebook:
                        user = dbContext.People.FirstOrDefault(p => p.OAuth2IdFacebook.Equals(userId));
                        break;
                }

                if (user != null)
                    return Request.CreateResponse(HttpStatusCode.OK, user);

                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
            catch (Exception)
            {

            }
            finally
            {
                dbContext.Dispose();
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [AuthorizationActionFilter]
        [RequireHttps]
        public HttpResponseMessage GetUserByName(string email)
        {
            var dbContext = new RSIContext();
            try
            {
                Models.Person user = null;

                user = dbContext.People.FirstOrDefault(p => p.Email.Equals(email) || p.PhoneNumber.Equals(email));

                if (user != null)
                {
                    var vmUser = new RSICommons.CommonModels.DTO.Person()
                    {
                        Id = user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Address = user.Address,
                        Email = user.Email,
                        PhoneNumber = user.PhoneNumber
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, vmUser);
                }
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            catch (Exception)
            {

            }
            finally
            {
                dbContext.Dispose();
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
