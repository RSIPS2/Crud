namespace CrudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OAuth2IdGoogle = c.String(),
                        OAuth2IdTwitter = c.String(),
                        OAuth2IdFacebook = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PersonProduct",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BuyTime = c.DateTime(nullable: false),
                        Amount = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        PersonId = c.Guid(nullable: false),
                        ProductId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.ProductId);
            
            CreateTable(
                "dbo.Product",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Title = c.String(),
                        Author = c.String(),
                        Description = c.String(),
                        Publisher = c.String(),
                        PublishDate = c.DateTime(nullable: false),
                        PageCount = c.Int(nullable: false),
                        Price = c.Int(nullable: false),
                        Genre = c.String(),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subscription",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Type = c.Int(nullable: false),
                        PersonId = c.Guid(nullable: false),
                        ProductId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Person", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("dbo.Product", t => t.ProductId)
                .Index(t => t.PersonId)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscription", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Subscription", "PersonId", "dbo.Person");
            DropForeignKey("dbo.PersonProduct", "ProductId", "dbo.Product");
            DropForeignKey("dbo.PersonProduct", "PersonId", "dbo.Person");
            DropIndex("dbo.Subscription", new[] { "ProductId" });
            DropIndex("dbo.Subscription", new[] { "PersonId" });
            DropIndex("dbo.PersonProduct", new[] { "ProductId" });
            DropIndex("dbo.PersonProduct", new[] { "PersonId" });
            DropTable("dbo.Subscription");
            DropTable("dbo.Product");
            DropTable("dbo.PersonProduct");
            DropTable("dbo.Person");
        }
    }
}
