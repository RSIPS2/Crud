namespace CrudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Product", "Language", c => c.String());
            AddColumn("dbo.Product", "OriginalLanguage", c => c.String());
            AddColumn("dbo.Product", "Thumbnail", c => c.String());
            AddColumn("dbo.Product", "TranslatedBy", c => c.String());
            AlterColumn("dbo.PersonProduct", "Price", c => c.String());
            AlterColumn("dbo.Product", "Price", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Product", "Price", c => c.Int(nullable: false));
            AlterColumn("dbo.PersonProduct", "Price", c => c.Int(nullable: false));
            DropColumn("dbo.Product", "TranslatedBy");
            DropColumn("dbo.Product", "Thumbnail");
            DropColumn("dbo.Product", "OriginalLanguage");
            DropColumn("dbo.Product", "Language");
        }
    }
}
