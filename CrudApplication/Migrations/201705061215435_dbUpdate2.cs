namespace CrudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dbUpdate2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subscription", "ProductId", "dbo.Product");
            DropIndex("dbo.Subscription", new[] { "ProductId" });
            AlterColumn("dbo.Product", "PublishDate", c => c.String());
            AlterColumn("dbo.Subscription", "ProductId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Subscription", "ProductId");
            AddForeignKey("dbo.Subscription", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscription", "ProductId", "dbo.Product");
            DropIndex("dbo.Subscription", new[] { "ProductId" });
            AlterColumn("dbo.Subscription", "ProductId", c => c.Guid());
            AlterColumn("dbo.Product", "PublishDate", c => c.DateTime(nullable: false));
            CreateIndex("dbo.Subscription", "ProductId");
            AddForeignKey("dbo.Subscription", "ProductId", "dbo.Product", "Id");
        }
    }
}
