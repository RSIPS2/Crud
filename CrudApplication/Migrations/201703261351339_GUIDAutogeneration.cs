namespace CrudApplication.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class GUIDAutogeneration : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PersonProduct", "PersonId", "dbo.Person");
            DropForeignKey("dbo.Subscription", "PersonId", "dbo.Person");
            DropForeignKey("dbo.PersonProduct", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Subscription", "ProductId", "dbo.Product");
            DropPrimaryKey("dbo.Person");
            DropPrimaryKey("dbo.PersonProduct");
            DropPrimaryKey("dbo.Product");
            DropPrimaryKey("dbo.Subscription");
            AlterColumn("dbo.Person", "Id", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"));
            AlterColumn("dbo.PersonProduct", "Id", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"));
            AlterColumn("dbo.Product", "Id", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"));
            AlterColumn("dbo.Subscription", "Id", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newsequentialid()"));
            AddPrimaryKey("dbo.Person", "Id");
            AddPrimaryKey("dbo.PersonProduct", "Id");
            AddPrimaryKey("dbo.Product", "Id");
            AddPrimaryKey("dbo.Subscription", "Id");
            AddForeignKey("dbo.PersonProduct", "PersonId", "dbo.Person", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Subscription", "PersonId", "dbo.Person", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PersonProduct", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Subscription", "ProductId", "dbo.Product", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscription", "ProductId", "dbo.Product");
            DropForeignKey("dbo.PersonProduct", "ProductId", "dbo.Product");
            DropForeignKey("dbo.Subscription", "PersonId", "dbo.Person");
            DropForeignKey("dbo.PersonProduct", "PersonId", "dbo.Person");
            DropPrimaryKey("dbo.Subscription");
            DropPrimaryKey("dbo.Product");
            DropPrimaryKey("dbo.PersonProduct");
            DropPrimaryKey("dbo.Person");
            AlterColumn("dbo.Subscription", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Product", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.PersonProduct", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Person", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Subscription", "Id");
            AddPrimaryKey("dbo.Product", "Id");
            AddPrimaryKey("dbo.PersonProduct", "Id");
            AddPrimaryKey("dbo.Person", "Id");
            AddForeignKey("dbo.Subscription", "ProductId", "dbo.Product", "Id");
            AddForeignKey("dbo.PersonProduct", "ProductId", "dbo.Product", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Subscription", "PersonId", "dbo.Person", "Id", cascadeDelete: true);
            AddForeignKey("dbo.PersonProduct", "PersonId", "dbo.Person", "Id", cascadeDelete: true);
        }
    }
}
