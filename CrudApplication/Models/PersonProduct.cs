﻿using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CrudApplication.Models
{
    public class PersonProduct
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public DateTime BuyTime { get; set; }
        public int Amount { get; set; }
        public string Price { get; set; }
        public Guid PersonId { get; set; }
        public Guid ProductId { get; set; }
        public virtual Person Person { get; set; }
        public virtual Product Product { get; set; }
    }
}