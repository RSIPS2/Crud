﻿using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CrudApplication.Models
{
    public class Person
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public string OAuth2IdGoogle { get; set; }
        public string OAuth2IdTwitter { get; set; }
        public string OAuth2IdFacebook { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<PersonProduct> PersonProducts { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }

    }
}