﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace CrudApplication.Models
{
    public class RSIContext : DbContext
    {
        public RSIContext() : base("RSIConnectionString")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

        public DbSet<Person> People { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<PersonProduct> ProductPersons { get; set; }

    }

    public class RSIInitializer : DropCreateDatabaseIfModelChanges<RSIContext>
    {
        protected override void Seed(RSIContext context)
        {
                       
        }
    }
}