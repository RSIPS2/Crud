﻿using RSICommons.CommonModels.DTO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CrudApplication.Models
{
    public class Product
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Publisher { get; set; }
        public string PublishDate { get; set; }
        public int PageCount { get; set; }
        public string Price { get; set; }
        public string Genre { get; set; }
        public string Language { get; set; }
        public string OriginalLanguage { get; set; }
        public string Thumbnail { get; set; }
        public string TranslatedBy { get; set; }
        public int Amount { get; set; }

        public virtual ICollection<PersonProduct> PersonProducts { get; set; }
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}